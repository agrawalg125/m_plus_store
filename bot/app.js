const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');

const app = express();

app.use(bodyParser.urlencoded({extended : true}));

app.listen(3000, function() {
  console.log("Server Running at port 3000");
});

app.post("/botTalk", function(req, res){
  const botIpAdd = "http://127.0.0.1:8000";
  const user_input= req.body.user_input;
  const url = botIpAdd + "/botTalk?user_input="+user_input;
  request(url, function(err, response, body){
    if (err){
      console.log(err);
      res.send("Some Error Occured. Try Again");
    }
    else{
      res.send(body);
    }
  });
});

const sql = require("./db.js");

// constructor
const Medicine = function(medicine) {
  this.store_id = medicine.store_id;
  this.medicine_name = medicine.medicine_name;
  this.manufacturer_name = medicine.manufacturer_name;
  this.quantity_per_pack = medicine.quantity_per_pack;
  this.quantity_in_stock = medicine.quantity_in_stock;
  this.expiry_date = medicine.expiry_date;
  this.manufacturing_date = medicine.manufacturing_date;
  this.description = medicine.description;
  this.discount = medicine.discount;
  this.selling_price_after_discount = medicine.selling_price_after_discount;


};

Medicine.create = (newMedicine, result) => {
  let insertionQuery = 'Call stores_add_medicine(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
  let data = [newMedicine.store_id, newMedicine.medicine_name, newMedicine.manufacturer_name, newMedicine.quantity_per_pack,
              newMedicine.quantity_in_stock, newMedicine.expiry_date, newMedicine.manufacturing_date, newMedicine.description, newMedicine.discount,
              newMedicine.selling_price_after_discount];
  sql.query(insertionQuery, data, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created medicine: ", { id: res.insertId, ...newMedicine });
    result(null, { id: res.insertId, ...newMedicine });
  });
};

// Medicine.getAll = result => {
//   sql.query("CALL select_all_medicines()", (err, res) => {
//     if (err) {
//       console.log("error: ", err);
//       result(err,null);
//       return;
//     }

//     console.log("Medicines: ", res);
//     result(null, res);
//   });
// };store_view_all_medicine_in_stores


Medicine.getAllForPatient = (medicine, result) => {
  console.log("in get all for patinet");
  let query = 'CALL patient_view_all_medicines()';
  sql.query(query, (err, res) => {
    if(err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    if(res.length) {
      result(null, res);
    }
    else
      result({kind:"not_found"}, null);
  });
};

Medicine.getAllForStore = (medicine, result) => {
  let query = 'CALL store_view_all_medicine_in_stores(?)';
  let data = [medicine.store_id];
  sql.query(query, data, (err, res) => {
    if(err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    if(res.length) {
      result(null, res);
    }
    else
      result({kind:"not_found"}, null);
  });
};

Medicine.findByNameForPatient = (name, result) => {
  let query = 'CALL patients_view_medicine_by_name(?)';
  let data = [name];
  sql.query(query, data, (err, res) => {
    if(err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    if(res.length) {
      result(null, res[0]);
    }
    else
      result({kind:"not_found"}, null);
  });
};

Medicine.findByNameForStore = (name, medicine, result) => {
  let query = 'CALL store_view_medicines_by_name(?, ?)';
  let data = [medicine.store_id, name];
  sql.query(query, data, (err, res) => {
    if(err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    if(res.length) {
      result(null, res[0]);
    }
    else
      result({kind:"not_found"}, null);
  });
};

Medicine.remove = (id, medicine, result) => {
  let query = 'CALL store_Delete_medicine(?, ?)';
  let data = [id, medicine.store_id];
  console.log("to delete medicine id = ",id," , store id = ",medicine.store_id);
  sql.query(query, data, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found store with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted medicine with id: ", id);
    result(null, res);
  });
};


Medicine.updateById = (id, medicine, result) => {
  let query = 'CALL store_update_medicine(?,?,?,?,?,?,?,?,?,?,?)';
  let data = [medicine.store_id, id, medicine.medicine_name, medicine.manufacturer_name, medicine.quantity_per_pack,
                medicine.quantity_in_stock, medicine.expiry_date, medicine.manufacturing_date, medicine.description, medicine.discount,
                medicine.selling_price_after_discount];
	sql.query(query, data, (err, res) => {
			if (err) {
		        console.log("error: ", err);
		        result(null, err);
		        return;
		  	}

		  	if(res.affectedRows == 0) {
		  		result({ kind: "not_found" }, null);
		  		return;
		  	}
		  	result(null, {id:id, ...medicine});
		}
	);
};


module.exports = Medicine;

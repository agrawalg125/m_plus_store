const sql = require("./db.js");

// constructor
const Cart = function(cart) {
  this.patientId = cart.patientId;
  this.medicineId = cart.medicineId;
  this.quantity = cart.quantity;
  this.total =cart.total;
  this.subtotal = cart.subtotal;
  this.tax = cart.tax;
  this.shipping_charges= cart.shipping_charges;
  this.grand_total = cart.grand_total;
};

Cart.create = (newCart, result) => {

  console.log("in cart model : \n medicineid = ",newCart.medicineId, ", quantity = ",newCart.quantity, ", patientid = ", newCart.patientId);

  let insertionQuery = 'Call add_to_cart(?, ?, ?)';
  let data = [newCart.medicineId, newCart.quantity, newCart.patientId];
  sql.query(insertionQuery, data, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created cart: ", { id: res.insertId, ...newCart });
    result(null, { id: res.insertId, ...newCart });
  });
};

/*Cart.getAll = result => {
  sql.query("SELECT * FROM carts", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("Cart: ", res);
    result(null, res);
  });
};*/

Cart.getAll = (patientId, result) => {
  let query = 'CALL view_cart(?)';
  let data = [patientId];
  sql.query(query, data, (err, res) => {
    if(err) {
      console.log("error: ", err);
          result(err, null);
          return;
    }

    if(res.length) {
      result(null, res[0]);
      return;
    }

    result({ kind: "not_found" }, null);
  });
};

Cart.update = (cart, result) => {

  let query = 'CALL update_cart(?, ?, ?)';
  let data = [cart.medicineId, cart.quantity, cart.patientId];
  sql.query(query, data, (err, res) => {
      if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if(res.affectedRows == 0) {
          result({ kind: "not_found" }, null);
          return;
        }
        result(null, {message:"updated"});
    }
  );
};

Cart.remove = (cart, result) => {
  console.log("in delete, medicineId = ", cart.medicineId, ", patientId = ", cart.patientId);
  let query = 'CALL delete_from_cart(?,?)';
  let data = [cart.medicineId, cart.patientId];
  sql.query(query, data, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found store with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted from cart , medicine with id: ", cart.medicineId);
    result(null, res);
  });
};

Cart.removeAll = (cart, result) => {
	let query = 'CALL delete_from_cart_after_checkout(?)';
	let data = [cart.patientId];
	sql.query(query, data, (err, res) => {
		if(err) {
			console.log("error: ", err);
			result(null, err);
			return;
		}
		if(res.affectedRows == 0) {
			result({ kind: "not_found" }, null);
			return;
		}
		console.log("delete cart, with patientId: ", cart.patientId);
		result(null, res);
	});
};

module.exports = Cart;

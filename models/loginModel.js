const sql = require("./db.js");

const Login = function(login) {
	this.email_id = login.email_id;
	this.pwd = login.pwd;
};

Login.postRequest = (email_id, result) => {

	let query = 'CALL password_retrieval(?)';
	let data = [email_id];

	sql.query(query, data, (err, res) => {
		if(err) {
    		console.log("error: ", err);
      		result(err, null);
      		return;
    	}
    	console.log("result from sql in loginModel(password_retrieval) = ",res[0][0].result);
    	result(null, res[0][0].result);
	});
};

Login.getRequest = (email_id, result) => {
	let query = 'CALL login_users(?)';
	let data = [email_id];

	sql.query(query, data, (err, res) => {
		if(err) {
			result(err, null);
			return;
		}
		console.log("result from sql in loginModel(login_users) = ",res[0][0]);
		result(null, res[0][0]);
	});
};

module.exports = Login;
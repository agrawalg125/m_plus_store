module.exports = app => {
    const feedback = require("../controllers/FeedbackController.js");

    console.log("See All Feedbacks route page");
    app.get("/users_seefeedback", feedback.getRequest);

  };
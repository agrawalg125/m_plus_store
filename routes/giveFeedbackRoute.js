module.exports = app => {
    const feedback = require("../controllers/FeedbackController.js");

    console.log("Submit Feedback route page");
    app.post("/users_givefeedback", feedback.postRequest);

  };
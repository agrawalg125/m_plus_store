module.exports = app => {
    const medicines = require("../controllers/medicineController.js");

    // Create a new medicines
    app.post("/medicines", medicines.create);

    // Retrieve all medicines
    app.get("/medicines", medicines.findAll);

    // Retrieve a single medicines with medicinesId
    app.get("/medicines/:medicineId", medicines.findOne);

    // Update a medicines with medicinesId
    app.put("/medicines/:medicineId", medicines.update);

    // Delete a medicines with medicinesId
    app.delete("/medicines/:medicineId", medicines.delete);

  };
  

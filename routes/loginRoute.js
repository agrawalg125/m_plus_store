module.exports = app => {
    const login = require("../controllers/loginController.js");

    console.log("Login route page");
    app.post("/login", login.postRequest);

  };

module.exports = app => {
  const medicine = require("../controllers/medicineController.js");

  // Create a new store
  app.post("/medicines", medicine.create);

  // Retrieve all stores
  app.get("/medicines", medicine.findAll);

  // Retrieve a single store with storeId
  app.get("/medicines/:medicineName", medicine.findOne);

  // Update a store with storeId
  app.put("/medicines/:medicineId", medicine.update);

  // Delete a store with storeId
  app.delete("/medicines/:medicineId", medicine.delete);

};

function parseData(body){
  const med=[
    body.medicine_id, body.store_id, body.medicine_name,
    body.manufacturer_name, body.quantity_per_pack,
    body.quantity_in_stock, body.expiry_date, body.manufacturing_date,
    body.description, body.discount,body.selling_price_after_discount
  ];
  return med;
}


module.exports = app => {
  const sql = require("../models/db.js");

  // // Retrieve all Medicines
  app.get("/medicines", function(req, res){
  	let query = 'Select * from Medicines';
  	sql.query(query, (err, meds) => {
  		if(err) {
  			console.log("error: ", err);
        		return;
  		}
      res.send(meds);
    });
  });

  app.post("/addMedicine", function(req, res){
    const newmed= parseData(req.body);
    let query= "INSERT INTO Medicines VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    sql.query(query, newmed, (err, meds) => {
  		if(err) {
  			console.log("error: ", err);
        		return;
  		}
      res.send('SUCCESS');
    });
  });

};

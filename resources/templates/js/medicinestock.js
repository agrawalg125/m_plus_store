var request;
$('#Add Medicine').click(function(event) {
   if(request) {
      request.abort();
   }
   window.location.href = "/medicines_add"
}

$('#Update').click(function(event) {
   if(request) {
      request.abort();
   }
   window.location.href = "/medicines_update"
}

// Integrating delete route with delete button

$('#Delete').click(function(event) {
   if(request) {
      request.abort();
   }
  
let store_id = $('#Store ID').val()
   let name = $('#Medicine Name').val()
   let Manufacturer = $('#Manufacturers Name').val()
   let perpack = $('#Quantity per pack').val()
   let Stock = $('#Quantity in Stock').val()
   let Edate = $('#Expiry Date').val()
   let Mdate = $('#Manufacturers Date').val()
   let Description = $('#Description').val()
   let Discount=$('#Discount(%').val()
   let sp=$('Selling Price after Discount').val()
     
   let data = 
   {
      store_id = store_id,
      medicine_name = name,
      manufacturer_name = Manufacturer ,
      quantity_per_pack = perpack ,
      quantity_in_stock = Stock ,
      expiry_date = Edate  ,
      manufacturing_date = Mdate ,
      description = Description ,
      discount = Discount ,
      selling_price_after_discount = sp
   }

   
   $.delete("medicine", data)
   .done(function(result)
    {
      if(result["message"] == "Not Authorised") {
         $('#error').text("You don't have enough permissions for adding this medicine.")
      }
      if(result["message"] == "No medicine with this id") {
         $('#error').text("No medicine with this id")
      }
      if(result["message"] == "Error occured") {
         $('#error').text("Some internal fault occured ! Please try again ")
      }
      else
      {
      $('#error').text("Medicine deleted successfully !")
      window.location.href = "/medicines_stock"
      }
    }
 }

// Integrating Add button of medadd.html with Create route ----> Post


$('#Add').click(function(event) {
   if(request) {
      request.abort();
   }
  
let store_id = $('#Store ID').val()
   let name = $('#Medicine Name').val()
   let Manufacturer = $('#Manufacturers Name').val()
   let perpack = $('#Quantity per pack').val()
   let Stock = $('#Quantity in Stock').val()
   let Edate = $('#Expiry Date').val()
   let Mdate = $('#Manufacturers Date').val()
   let Description = $('#Description').val()
   let Discount=$('#Discount(%').val()
   let sp=$('Selling Price after Discount').val()
     
   let data = 
   {
      store_id = store_id,
      medicine_name = name,
      manufacturer_name = Manufacturer ,
      quantity_per_pack = perpack ,
      quantity_in_stock = Stock ,
      expiry_date = Edate  ,
      manufacturing_date = Mdate ,
      description = Description ,
      discount = Discount ,
      selling_price_after_discount = sp
   }

   $.post("medicine", data)
   .done(function(result)
    {
      if(result["message"] == "Content can not be empty!") {
         $('#error').text("Please fill the neccessary fields !")
      }
      if(result["message"] == "Not suthorised") {
         $('#error').text("You don't have enough permissions for adding the medicine")
      }
      if(result["message"] == "Some error occurred while creating the medicine.") {
         $('#error').text("Some internal fault occured ! Please try again ")
      }
      else
      {
      $('#error').text("Medicine added successfully !")
      window.location.href = "/medicines_stock"
      
      }
    }
 }


 // Integrating Update button of medupdate.html with Create route ----> Put

$('#Update').click(function(event) {
   if(request) {
      request.abort();
   }
  
let store_id = $('#Store ID').val()
   let name = $('#Medicine Name').val()
   let Manufacturer = $('#Manufacturers Name').val()
   let perpack = $('#Quantity per pack').val()
   let Stock = $('#Quantity in Stock').val()
   let Edate = $('#Expiry Date').val()
   let Mdate = $('#Manufacturers Date').val()
   let Description = $('#Description').val()
   let Discount=$('#Discount(%').val()
   let sp=$('Selling Price after Discount').val()
     
   let data = 
   {
      store_id = store_id,
      medicine_name = name,
      manufacturer_name = Manufacturer ,
      quantity_per_pack = perpack ,
      quantity_in_stock = Stock ,
      expiry_date = Edate  ,
      manufacturing_date = Mdate ,
      description = Description ,
      discount = Discount ,
      selling_price_after_discount = sp
   }

   
   $.put("medicine", data)
   .done(function(result)
    {
      if(result["message"] == "Empty Content") {
         $('#error').text("You haven't filled the datails yet ! ")
      }
      if(result["message"] == "No medicine with this id") {
         $('#error').text("No medicine with this id")
      }
      if(result["message"] == "Error occured") {
         $('#error').text("Some internal fault occured ! Please try again ")
      }
      else
      {
      $('#error').text("Medicine details updated successfully !")
      window.location.href = "/medicines_stock"
      
      }
    }
 }

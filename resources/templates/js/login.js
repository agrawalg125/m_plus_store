var request;
$('#loginButton').click(function(event) {
   if(request) {
      request.abort();
   }
   let email = $('#username').val()
   let password = $('#password').val()

   let data = {
      email_id : email,
      pwd : password
   }
   $.post("login", data)
      .done(function(result) {
         if(result["message"] == "USER NOT FOUND") {
            $('#error').text("You may be entering an invalid email")
         } else if(result["message"] == "INCORRECT PASSWORD") {
            $('#error').text("Your password is incorrect")
         } else {
            if(result["user_type"] == 1) {
               window.location.href = "/medicine_stock"
            } else {
               window.location.href = "/medic"
            }
         }
      })
})

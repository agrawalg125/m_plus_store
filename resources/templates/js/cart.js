
var request;

var subTotal=0.0,tax=0.05,shipping=15,grandTotal=0;
var medi = []
var prescription_list = []


$(window).on('load', function() {

	console.log("document.cookie = ",document.cookie);

	let data = {
		cookie: document.cookie
	}

	let medicineId="",medicineName="";
	let manufacturerName="";
	let manufacturingDate="";
	let expiryDate="";
	let discount="";
	let sellingPriceAfterDiscount="";
	let quantity="";
	$.get('/cart')
		.done(function(result) {
			result.forEach(res => {
				medicineId=res.medicine_id;
			 medicineName=res.medicine_name;
			 manufacturerName=res.manufacturer_name;
			 manufacturingDate=res.manufacturing_date;
			 expiryDate=res.description;
			 discount=res.discount;
			 sellingPriceAfterDiscount=res.selling_price_after_discount;
			 quantity=res.quantity;
			 //
			 //console.log("type is  \n"+typeof res.selling_price_after_discount);
			 subTotal=subTotal+sellingPriceAfterDiscount;
			 let id = "medicine"+medicineId;
			 // let tmp = '<div class="product"'+'id='+id+'><div class="product-image"><img src="https://images.pexels.com/photos/2449364/pexels-photo-2449364.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=300"></div><div class="product-details"><div class="product-title white_text_bold">'+medicineName+'</div><p class="product-description white_text">'+ "Manufacturer : "+manufacturerName +'</p></div><div class="product-price" id="orgprice'+medicineId+'">'+sellingPriceAfterDiscount+'</div><div class="product-quantity"><input type="number" value="1" min="1" id="qty'+medicineId+'" onchange="changeQty(this)"></div><div class="product-removal"><button class="remove-product" type="button" id="remove1" value="'+id+'" onclick="removeproduct(this)">Remove</button></div><div class="product-line-price" id="totalprice'+medicineId+'">'+sellingPriceAfterDiscount+'</div></div>';
			 // $('.column-labels').after($(tmp))
			 let tmp = '<div class="product"'+'id='+id+'><div class="product-image"><img src="https://images.pexels.com/photos/2449364/pexels-photo-2449364.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=300"></div><div class="product-details"><div class="product-title white_text_bold">'+medicineName+'</div><p class="product-description white_text">'+ "<b>Manufacturer</b> : "+manufacturerName +'</p></div><div class="product-price white_text" id="orgprice'+medicineId+'">'+sellingPriceAfterDiscount+'</div><div class="product-quantity"><input type="number" value="1" min="1" id="qty'+medicineId+'" onchange="changeQty(this)"></div><div class="product-removal"><button class="remove-product" type="button" id="remove1" value="'+id+'" onclick="removeproduct(this)">Remove</button></div><div class="product-line-price white_text" id="totalprice'+medicineId+'">'+sellingPriceAfterDiscount+'</div></div>';
			 $('.column-labels').after($(tmp))
		 });
		})
		.done(function(res) {
			//console.log("\n",subTotal);
			document.getElementById("cart-subtotal").innerHTML=subTotal;
			let temp=subTotal+subTotal*0.05+15;
			document.getElementById("cart-total").innerHTML=temp;
			document.getElementById("cart-tax").innerHTML=subTotal*0.05;
			grandTotal = temp
		})




});

function removeproduct(val)
{
	document.getElementById(val.value).remove();
	let medid = (val.value).slice(8);
	let data1 ={
		medicineId : medid
	}
	$.ajax({
		   url: '/cart',
		   method: 'DELETE',
		   data: data1,

		   success: function(response) {
		     console.log(response["message"]);
		   }
	});

}

function changeQty(val)
{
	let medid = (val.id).slice(3);
	console.log("medid = ",medid);
	let orgp = parseInt(document.getElementById('orgprice'+medid).innerHTML);
	console.log("orgp = ",orgp);
	console.log(document.getElementById('totalprice'+medid).innerHTML);

	let s1 = document.getElementById('totalprice'+medid).innerHTML;
	document.getElementById('totalprice'+medid).innerHTML=orgp*(val.value);
	let s2 = document.getElementById('totalprice'+medid).innerHTML;

	if(s2 < s1) {
		subTotal-=(s1-s2);
	}
	else {
		subTotal+=(s2-s1);
	}

	console.log("value is ",val.value);


	document.getElementById("cart-subtotal").innerHTML=subTotal;
	let temp=subTotal+subTotal*0.05+15;
	document.getElementById("cart-total").innerHTML=temp;
	document.getElementById("cart-tax").innerHTML=subTotal*0.05;
	grandTotal = temp;

	let data1 = {
		medicineId : medid,
		quantity : val.value
	}

	$.ajax({
		   url: '/cart',
		   method: 'PUT',
		   data: data1,

		   success: function(response) {
		     console.log(response["message"]);
		   }
	});
}

function checkout() {
	let medicines = document.getElementsByClassName('product')
	console.log(medicines)


	for(let i = 0; i < medicines.length; i++) {
		let id = "qty" + (medicines[i].id).slice(8)
		let eachQty = document.getElementById(id).value
		let price = parseInt(document.getElementById("totalprice"+(medicines[i].id).slice(8)).innerHTML)

		console.log("price value = ",price)

		medi.push({
			medicineID : (medicines[i].id).slice(8),
			quantity : eachQty,
			medPrice : price
		})
	}
	console.log("medi = ",medi);

	var x = document.getElementById("files");
	var txt = "";
	console.log("x = ",x);

	if ('files' in x) {
	  if (x.files.length == 0) {
	    txt = "Select one or more files.";
	  } else {
	  	prescription_list.push(x.files)
	    // for (var i = 0; i < x.files.length; i++) {
	    //   txt += "<br><strong>" + (i+1) + ". file</strong><br>";
	    //   var file = x.files[i];
	    //   console.log("x.file = ",x.files[i]);
	    //   if ('name' in file) {
	    //     txt += "name: " + file.name + "<br>";
	    //   }
	    //   if ('size' in file) {
	    //     txt += "size: " + file.size + " bytes <br>";
	    //   }
	    // }
	  }
	}
	console.log("prescription_list = ",prescription_list[0]);


	var i;
	var name_list = []
	for(i=0;i<x.files.length;i++) {

		console.log("x.files at ",i," = ",x.files[i]);
		name_list.push(x.files[i].name)

		const formData = new FormData()
		formData.append('prescription',x.files[i])

		fetch('/upload', {
		    method: 'POST',
		    body: formData
		  })
		.then(response => response.json())
		  .then(data => {
		    console.log(data.path)
		  })
		  .catch(error => {
		    console.error(error)
		  })
	}
	console.log("name list = ",name_list);
  	//formData.append('prescription', x.files[0])



	var d=new Date();
	var nd = d.toISOString().split("T")[0];
	//var nd=(d.getFullYear()).toString()+"-"+(d.getMonth()).toString()+"-"+(d.getDay()).toString();

	   var paddress_line_1 = $('#address_line_1').val()
	   var paddress_line_2 = $('#address_line_2').val()
	   var pcity = $('#city').val()
	   var pstate = $('#state').val()
	   var pcountry = $('#country').val()
	   var ppincode = $('#pincode').val()

	   var paddress_line_1="aa"
	   var paddress_line_2="bb"
	   var pcity="cc"
	   var pstate="dd"
	   var pcountry="ee"
	   var ppincode=11


	let data1={
		date_of_order:nd,
		total_price:grandTotal,
		medicines:medi,
		prescription:name_list,
		isCOD:1,
		addressFlag:0,
		address_line_1: paddress_line_1,
	    address_line_2: paddress_line_2,
	    city: pcity,
	    state: pstate,
	    country: pcountry,
	    pincode: ppincode
	}
	console.log("data1 = ",data1);

	var pdata = {}

	$.post("/orders",data1)
		.done(function(result) {
			console.log("result = ",result[0].result);
			// pdata["order_id"]=result[0].result
			// pdata["amount"]=grandTotal
			// console.log("pdata = ",pdata);
			// $.get("/payment",pdata)
			var str = "/payment?order_id="+result[0].result+"&amount="+grandTotal
			window.location.href = str

		})
		// .done(function(result) {
		// 	$.get("/payment",pdata)
}

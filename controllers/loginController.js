const Login = require("../models/loginModel.js");
const bcrypt = require('bcrypt');
const cookieParser = require('cookie-parser');
const saltRounds = 10;

exports.postRequest = async (req, res) => {
	if(!req.body) {
		res.status(400).send({
			message: "Empty content"
		});
	}


	const password = req.body.pwd;
  	const encryptedPassword = await bcrypt.hash(password, saltRounds);

  	const login = new Login({
  		email_id: req.body.email_id,
  		pwd: encryptedPassword
  	});

  	Login.postRequest(req.body.email_id, (err, data) => {
  		console.log("data from controller(password retrieval) = ",req.body.email_id);
  		if(err)
  			res.status(500).send({
  				message:
  					err.message || "Error occured"
  			});
  		else {

  			console.log("data returned to loginController = ",data);
  			if(data == "USER NOT FOUND") {
  				res.send({message: "USER NOT FOUND"});
  			}

  			else {
  				bcrypt.compare(password, data, function(err, result) {
	  				if(result == true) {
	  					// console.log("Password is correct");
	  					//res.send({message: "Password is correct"});
	  					Login.getRequest(req.body.email_id, (err, data) => {
	  						if(err)
								res.status(500).send({
									message:
										err.message || "Error occured"
								});
							else {
								if(data.user_type === 1) {
									console.log('The id is',data.store_id)
									res.cookie('storeID', data.store_id, {maxAge: 900000, httpOnly: true});
									res.send({message: "SUCCESSFUL", user_type: data.user_type, user_id: data.store_id});
								}
								else {
									res.cookie('patientID', data.patient_id, {maxAge: 900000, httpOnly: true});
									res.send({message: "SUCCESSFUL", user_type: data.user_type, user_id: data.patient_id});
								}
							}
	  					});
	  				}
	  				else {
	  					console.log("Incorrect password");
	  					res.send({message: "INCORRECT PASSWORD"});
	  				}
	  			});
  			}


  		}
  	});

};

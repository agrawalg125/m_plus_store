const Order = require("../models/orderModel.js");

// Create and Save a new order
exports.create = (req, res) => {
   // Validate request
   if (!req.body) {
     res.status(400).send({
       message: "Content can not be empty!"
     });
   }

   // Create a order
   const order = new Order({
     date_of_order: req.body.date_of_order,
     total_price: req.body.total_price,
     isCOD:req.body.isCOD,
     medicines:req.body.medicines,
     prescription: req.body.prescription,
     addressFlag :req.body.addressFlag,
     address_line_1: req.body.address_line_1,
     address_line_2: req.body.address_line_2,
     city: req.body.city,
     state: req.body.state,
     country: req.body.country,
     pincode: req.body.pincode
   });

   // Save order in the database
   Order.create(order, (err, data) => {
     if (err)
       res.status(500).send({
         message:
           err.message || "Some error occurred while creating the Order."
       });
     else res.send(data);
   });
};

// Retrieve all order from the database.
exports.findAll = (req, res) => {
   Order.getAll((err, data) => {
   if (err)
    res.status(500).send({
     message:
        err.message || "Some error occurred while retrieving order."
    });
    else
    {
      //console.log(data);
      res.send(data);
    }
});
};



// Find a single order with a orderId
exports.find = (req, res) => {
  if(req.params.IdFlag)
  {
      Order.findById(req.params.orderId, (err, data) => {
      if(err) {
        if(err.kind === 'not_found') {
          res.status(404).send({
            message:
            `No order found with this id ${req.params.orderId}`
          });
        } 	else {
          res.status(500).send({
            message:
            "Error getting order with this id" + req.params.orderId
          });
        }
      }	else res.send(data);
    });
  }
  else
  {
    Order.findPrevOrderById(req.params.patientId, (err, data) => {
    if(err) {
      if(err.kind === 'not_found') {
        res.status(404).send({
          message:
          `No  previous orders found with this id ${req.params.patientId}`
        });
      } 	else {
        res.status(500).send({
          message:
          "Error getting previous orders with this id" + req.params.patientId
        });
      }
    }	else res.send(data);
  });
  }
};

exports.viewOrders = (req, res) => {
   let resultData = {
      presentOrders : [],
      pastOrders : []
   };
   console.log("cookie in viewOrders = ",req.cookies);
    let str = req.headers.cookie;
    console.log("cookie in headers = ",str);
    let parts = str.split('=');
    let id = parseInt(parts[1]);

//   if(req.cookies.storeID !== undefined) {
    if(parts[0]==="storeID") {
      console.log("we have store id in cookie");
         let info = {
          //  storeID: req.cookies.storeID
          storeID: id
         }
         Order.getCurrentOrdersForStore(info, ( err, data) => {
         if (err)
          res.status(500).send({
           message:
              err.message || "Some error occurred while retrieving order."
          });
          else
          {
            //console.log(data);
            resultData.presentOrders = data;
            Order.getPastOrdersForStore(info, ( err, data) => {
            if (err)
             res.status(500).send({
              message:
                 err.message || "Some error occurred while retrieving order."
             });
             else
             {
                resultData.pastOrders = data;
                res.send(resultData)
             }
          });
          }
      });
   } else {
      console.log("We have patient id in cookie");
      let info = {
       //  patientID: req.cookies.patientID
       patientID: id
      }
      Order.getCurrentOrdersForPatient(info, (err, data) => {
      if (err)
      res.status(500).send({
        message:
           err.message || "Some error occurred while retrieving order."
      });
      else
      {
         //console.log(data);
         // res.send(data);
         resultData.presentOrders = data;
         Order.getPastOrdersForPatient(info, (err, data) => {
            if (err)
            res.status(500).send({
              message:
                 err.message || "Some error occurred while retrieving order."
            });
            else
            {
               resultData.pastOrders = data;
               res.send(resultData);
            }

         })
      }
   });
   }

};

// Delete a order with the specified orderId in the request
exports.delete = (req, res) => {
  Order.remove(req.params.orderId, (err, data) => {
		if(err) {
			if(err.kind === "not_found") {
				res.status(404).send({
					message:`No order with this id ${req.params.orderId}`
				});
			}	else {
				res.status(500).send({
					message:
						"Error in deleting order with this id "+req.params.orderId
				});
			}
		}	else res.send({message: `Order deleted`});
	});

};

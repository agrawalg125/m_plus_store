const Medicine = require("../models/medicineModel.js");

// Create and Save a new Medicine
exports.create = (req, res) => {

	let str = req.headers.cookie;
	let parts = str.split('=');
	let id = parseInt(parts[1]);

	if(parts[0]=="storeID") {
		if (!req.body) {
		   	res.status(400).send({
		   		message: "Content can not be empty!"
		   	});
	    }

	   // Create a Medicine
	   const medicine = new Medicine({
	   	store_id: id,
	   	medicine_name: req.body.medicine_name,
	   	manufacturer_name: req.body.manufacturer_name,
	   	quantity_per_pack: req.body.quantity_per_pack,
	   	quantity_in_stock: req.body.quantity_in_stock,
	   	expiry_date: req.body.expiry_date,
	   	manufacturing_date: req.body.manufacturing_date,
	   	description: req.body.description,
	   	discount: req.body.discount,
	   	selling_price_after_discount: req.body.selling_price_after_discount
	   });

	   // Save Medicine in the database
	   Medicine.create(medicine, (err, data) => {
		   	if (err)
		   		res.status(500).send({
		   			message:
		   			err.message || "Some error occurred while creating the medicine."
		   		});
		   	else res.send(data);
	   });
	}
	else {
		res.send({message:`Not authorized`});
	}
   // Validate request

};

// Retrieve all Medicines from the database.
exports.findAll = (req, res) => {

	let str = req.headers.cookie;
	let parts = str.split('=');
	let id = parseInt(parts[1]);

	console.log("cookie in controller findAll = ",parts[0]," ",parts[1]);
	console.log("parts 0 =",parts[0]);
	console.log("parts 1 =",parts[1]);

	const medicine = new Medicine({
	   	store_id: id,
	   	medicine_name: req.body.medicine_name,
	   	manufacturer_name: req.body.manufacturer_name,
	   	quantity_per_pack: req.body.quantity_per_pack,
	   	quantity_in_stock: req.body.quantity_in_stock,
	   	expiry_date: req.body.expiry_date,
	   	manufacturing_date: req.body.manufacturing_date,
	   	description: req.body.description,
	   	discount: req.body.discount,
	   	selling_price_after_discount: req.body.selling_price_after_discount
	});

	if(parts[0]=="patientID") {
		console.log("we have patient id in cookie");
		Medicine.getAllForPatient(medicine, (err, data) => {
			if (err)
				res.status(500).send({
					message:
					err.message || "Some error occurred while retrieving medicine."
				});
			else res.send(data);
		});
	}
	else {
		console.log("we have store id in cookie");
		Medicine.getAllForStore(medicine, (err, data) => {
			if(err)
				res.status(500).send({
					message:
					err.message || "Error encountered"
				});
			else res.send(data);
		});
	}

};

// Find a single Medicine with a MedicineId
exports.findOne = (req, res) => {

	let str = req.headers.cookie;
	let parts = str.split('=');
	let id = parseInt(parts[1]);

	const medicine = new Medicine({
	   	store_id: id,
	   	medicine_name: req.body.medicine_name,
	   	manufacturer_name: req.body.manufacturer_name,
	   	quantity_per_pack: req.body.quantity_per_pack,
	   	quantity_in_stock: req.body.quantity_in_stock,
	   	expiry_date: req.body.expiry_date,
	   	manufacturing_date: req.body.manufacturing_date,
	   	description: req.body.description,
	   	discount: req.body.discount,
	   	selling_price_after_discount: req.body.selling_price_after_discount
	});

	if(parts[0]=="patientID") {
		Medicine.findByNameForPatient(req.params.medicineName, (err, data) => {
			if(err) {
				if(err.kind === 'not_found') {
					res.status(404).send({
						message:
						`No store found with this id ${req.params.MedicineId}`
					});
				} 	else {
					res.status(500).send({
						message:
						"Error getting customer with this id" + req.params.MedicineId
					});
				}
			}	else res.send(data);
		});
	}
	else {
		Medicine.findByNameForStore(req.params.MedicineName, medicine, (err, data) => {
			if(err) {
				if(err.kind === 'not_found') {
					res.status(404).send({
						message:
						`No store found with this id ${req.params.MedicineId}`
					});
				} 	else {
					res.status(500).send({
						message:
						"Error getting customer with this id" + req.params.MedicineId
					});
				}
			}	else res.send(data);
		});
	}

};

// Update a Medicine identified by the MedicineId in the request
exports.update = (req, res) => {

	let str = req.headers.cookie;
	let parts = str.split('=');
	let id = parseInt(parts[1]);

	if(parts[0]=="storeID") {
		if(!req.body) {
			res.status(400).send({
				message:"Empty content"
			});
		}

		const medicine = new Medicine({
		   	store_id: id,
		   	medicine_name: req.body.medicine_name,
		   	manufacturer_name: req.body.manufacturer_name,
		   	quantity_per_pack: req.body.quantity_per_pack,
		   	quantity_in_stock: req.body.quantity_in_stock,
		   	expiry_date: req.body.expiry_date,
		   	manufacturing_date: req.body.manufacturing_date,
		   	description: req.body.description,
		   	discount: req.body.discount,
		   	selling_price_after_discount: req.body.selling_price_after_discount
	    });

		Medicine.updateById(req.params.medicineId, medicine, (err, data) => {
			if(err) {
				if(err.kind === "not_found") {
					res.status(404).send({
						message: `No medicine with id ${req.params.MedicineId}`
					});
				}
				else {
					res.status(500).send({
						message:
						err.message || "Error occured"
					});
				}
			}	else res.send(data);
		});
	}
	else {
		res.send({message: `Not authorized`});
	}

};

// Delete a Medicine with the specified MedicineId in the request
exports.delete = (req, res) => {

	let str = req.headers.cookie;
	let parts = str.split('=');
	let id = parseInt(parts[1]);

	if(parts[0]=="storeID") {

		const medicine = new Medicine({
		   	store_id: id,
		   	medicine_name: req.body.medicine_name,
		   	manufacturer_name: req.body.manufacturer_name,
		   	quantity_per_pack: req.body.quantity_per_pack,
		   	quantity_in_stock: req.body.quantity_in_stock,
		   	expiry_date: req.body.expiry_date,
		   	manufacturing_date: req.body.manufacturing_date,
		   	description: req.body.description,
		   	discount: req.body.discount,
		   	selling_price_after_discount: req.body.selling_price_after_discount
	    });

		Medicine.remove(req.params.medicineId, medicine, (err, data) => {
			if(err) {
				if(err.kind === "not_found") {
					res.status(404).send({
						message:`No medicine with this id ${req.params.MedicineId}`
					});
				}	else {
					res.status(500).send({
						message:
						"Error in deleting medicine with this id "+req.params.MedicineId
					});
				}
			}	else res.send({message: `Medicine deleted`});
		});
	}
	else {
		res.send({message:`Not authorized`});
	}

};
